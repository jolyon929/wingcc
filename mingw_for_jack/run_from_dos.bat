@echo off

@echo * append the path to the compiler
set PATH=%PATH%;%~dp0MinGW\bin

@echo * launch the compiler
gcc -o hello hello.c

@echo * run the executable
hello.exe
